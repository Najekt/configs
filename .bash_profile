# ~/.bash_profile
# Sourced by bash when used as a login shell

# Automatically run startx when logging in on tty1
[[ ! $DISPLAY && $XDG_VTNR -eq 1 ]] && exec startx
# Fallback when X not available
[[ -f ~/.bashrc ]] && . ~/.bashrc
