#!/usr/bin/env bash

DIR="$(dirname "${BASH_SOURCE}")"

sudo pacman -Syu

#xorg
sudo pacman -S --needed --noconfirm xorg-server xorg-xinit openbox obconf nitrogen tint2 gsimplecal arandr terminator htop pcmanfm-gtk3 gvfs xdg-user-dirs catfish lxappearance-gtk3 qt5ct
sudo pacman -S --needed --noconfirm arc-gtk-theme arc-icon-theme
#notify-osd?

# gnome-polkit
#/usr/lib/polkit-gnome/...

# Audio
sudo pacman -S --needed --noconfirm pulseaudio pulseaudio-alsa pavucontrol volumeicon
#alsa and udev built in kernel

# More
#catfish tumblerd? xarchiver

#temp
sudo pacman -S --needed --noconfirm virtualbox-guest-utils atom chromium

# Install .config
cp -r "${DIR}/.config" "${HOME}/"
curl "https://raw.githubusercontent.com/Antergos/wallpapers-extra/master/reflection.jpg" > "${HOME}/BG.jpg"
nitrogen --save --set-zoom-fill ~/BG.jpg

# Xinitrc
cp "${DIR}/.xinitrc" "${HOME}/"
cp "${DIR}/.bash_profile" "${HOME}/"
sudo mkdir -p '/etc/systemd/system/getty@tty1.service.d'
printf "[Service]\nExecStart=\nExecStart=-/usr/bin/agetty --autologin ${USER} --noclear %%I \$TERM\n" | sudo tee '/etc/systemd/system/getty@tty1.service.d/override.conf'

# Record installed packages
pacman -Qqet > installedpkglist.txt

# Restart
shutdown -r now
